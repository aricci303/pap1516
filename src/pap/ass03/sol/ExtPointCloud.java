package pap.ass03.sol;

import java.util.function.Consumer;

import pap.ass03.*;

public interface ExtPointCloud extends PointCloud {

	void apply(Consumer<P2d> t);
}
