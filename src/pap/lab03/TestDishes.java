package pap.lab03;

import java.util.Arrays;
import java.util.List;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class TestDishes {

	public static void main(String[] args) {
	    final List<Dish> menu =
	            Arrays.asList( new Dish("pork", false, 800, Dish.Type.MEAT),
	                           new Dish("beef", false, 700, Dish.Type.MEAT),
	                           new Dish("chicken", false, 400, Dish.Type.MEAT),
	                           new Dish("french fries", true, 530, Dish.Type.OTHER),
	                           new Dish("rice", true, 350, Dish.Type.OTHER),
	                           new Dish("season fruit", true, 120, Dish.Type.OTHER),
	                           new Dish("pizza", true, 550, Dish.Type.OTHER),
	                           new Dish("prawns", false, 400, Dish.Type.FISH),
	                           new Dish("salmon", false, 450, Dish.Type.FISH));

	    // contare il numero di dish vegetariani
	    
	    long nVeg = menu.stream()
	    	.filter(d -> d.isVegetarian())
	    	.count();
	    
	    System.out.println(nVeg);
	    
	    // stampare in ordine crescente 
	    // tutti i dish che hanno un numero di calorie comprese tra 400 e 600
	    
	    List<String> lowCal = menu.stream()
	    	.filter(d -> d.getCalories() >= 400 && d.getCalories() <= 600)
	    	.map(d -> d.getName())
	    	.sorted().collect(toList());
	    
	    lowCal.stream().forEach(System.out::println);
	    
	    // Dato un pasto formato dalle pietanze specificate in  myChoice (a seguire), 
	    // determinare la quantità di calorie complessive del pasto
	    
	    List<String> myChoice = Arrays.asList("chicken","rice","season fruit");
	    
	}

}
