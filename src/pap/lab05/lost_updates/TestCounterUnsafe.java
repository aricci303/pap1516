package pap.lab05.lost_updates;

public class TestCounterUnsafe {

	public static void main(String[] args) throws Exception {
		long startTime = System.currentTimeMillis();
		int ntimes = Integer.parseInt(args[0]);
		UnsafeCounter c = new UnsafeCounter(0);
		Worker w1 = new Worker(c,ntimes);
		Worker w2 = new Worker(c,ntimes);
		w1.start();
		w2.start();
		w1.join();
		w2.join();
		long elapsed = System.currentTimeMillis() - startTime;
		System.out.println("Counter final value: "+c.getValue()+" in "+elapsed+"ms.");
	}
}
